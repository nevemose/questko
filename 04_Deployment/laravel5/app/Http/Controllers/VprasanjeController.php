<?php

namespace App\Http\Controllers;

use App\Vprasanje;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Session;

class VprasanjeController extends Controller
{
    /**
     * Show a list of all available flights.
     *
     * @return Response
     */
    public function index()
    {
 
	$vprasanja =DB::table('vprasanje')->get();
	$i=0;
	foreach($vprasanja as $vpr){
		$uporabniki[$i]= DB::table('uporabnik')->select('uporabnisko_ime')->where('id_uporabnik', $vpr->UPORABNIK_id_uporabnik)->get();
		$i++;
	}
	$kategorije=DB::table('kategorija')->get();
	$upo=Session::get('uporabnisko_ime');
	return view('index')-> with(array('vprasanja' => $vprasanja))-> with(array('uporabniki' => $uporabniki))-> with(array('kategorija' => $kategorije))->with('uporabnik', $upo);

    }
	public function vprasaj()
    {
	$input = Input::only('name','message');            
   
    $name = $input['name'];
    $besedilo = $input['message'];
	DB::insert('insert into vprasanje (naslov_vprasanja, besedilo, ocenjenost, UPORABNIK_id_uporabnik) values (?, ?, ?, ?)', array($name, $besedilo, 8, 1));
	
	
	$vprasanja =DB::table('vprasanje')->get();
	$i=0;
	foreach($vprasanja as $vpr){
		$uporabniki[$i]= DB::table('uporabnik')->select('uporabnisko_ime')->where('id_uporabnik', $vpr->UPORABNIK_id_uporabnik)->get();
		$i++;
	}
	$kategorije=DB::table('kategorija')->get();
	$upo=Session::get('uporabnisko_ime');
	return view('index')-> with(array('vprasanja' => $vprasanja))-> with(array('uporabniki' => $uporabniki))-> with(array('kategorija' => $kategorije))->with('uporabnik', $upo);

    }
}