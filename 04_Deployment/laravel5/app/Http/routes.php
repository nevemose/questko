<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/','VprasanjeController@index');
Route::get('/oprojektu','Front@oprojektu');
Route::get('/vprasaj','Front@vprasaj');
Route::get('/vprasaj/1','VprasanjeController@vprasaj');
Route::get('/prijava','Front@prijava');
Route::get('/registracija/1','RegistracijaController@vnos');
Route::get('/registracija','Front@registracija');
Route::get('/odgovori','VprasanjeController@index');

Route::post('/prijava/1','SessionsController@create');


Route::get('blade', function () {
    $drinks = array('Vodka','Gin','Brandy');
    return view('page',array('name' => 'The Raven','day' => 'Friday','drinks' => $drinks));
	
});







