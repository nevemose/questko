<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vprasanje extends Model
{
     protected $table = 'vprasanje';
	 protected $primaryKey='id_vprasanja';
	 public static $naslov_v='naslov_vprasanja';
}
