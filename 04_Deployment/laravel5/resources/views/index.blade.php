@extends('layouts.master')

@section('header')
	@parent

    <section id="home-slider">
        <div class="container">
            <div class="row">
                <div class="main-slider">
                    <div class="slide-text">
                    	<h1>Za vse vsevednike</h1> <?php echo $uporabnik ?> 
                    	<input type="text" class="search-form" autocomplete="off" placeholder="Išči med vprašanji"><a href="#" class="btn btn-common">Išči</a>
                    	<input type="text" class="search-form" autocomplete="off" placeholder="Postavi vprašanje"><a href="#" class="btn btn-common">Postavi vprašanje</a>                      
                    </div>
                    <img src="images/home/slider/mark3.jpg" id="hide" class="slider-sun" alt="slider image">
                </div>
            </div>
        </div>
        <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
    </section>
    <!--/#home-slider-->
@stop
     <!--/#action-->
@section('content')
    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                        <div class="sidebar-item  recent">
                            <h3>Zadnji odgovori</h3>
                            <div class="media">
                                <div class="pull-left">
                                    <a href="#"><img src="images/portfolio/project1.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                                    <p>posted on  07 March 2014</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <a href="#"><img src="images/portfolio/project2.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                                    <p>posted on  07 March 2014</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <a href="#"><img src="images/portfolio/project3.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipisicing elit,</a></h4>
                                    <p>posted on  07 March 2014</p>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-item categories">
                            <h3>Kategorije</h3>
                            <ul class="nav navbar-stacked">
                            	<?php $k=0; foreach ($kategorija as $kat){
                                 if($k!=1){ ?>
                                 	<li><a href="#"><?php echo $kat->naziv; ?><span class="pull-right">(<?php echo $k+3 ?>)</span></a></li>
                               <?php }else{ ?>
                                <li class="active"><a href="#"><?php echo $kat->naziv; ?><span class="pull-right">(<?php echo $k+1 ?>)</span></a></li>
                                <?php }$k++;} ?>
                            </ul>
                        </div>
                        <div class="sidebar-item tag-cloud">
                            <h3>Oznake</h3>
                            <ul class="nav nav-pills">
                                <li><a href="#">
                                </a></li>
                                <li><a href="#">krasno</a></li>
                                <li><a href="#">vreme</a></li>
                                <li><a href="#">policija</a></li>
                                <li><a href="#">posel</a></li>
                                <li><a href="#">obogateti</a></li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <div class="col-md-9 col-sm-7">
                    <div class="row"> 
                			<?php $j=0; foreach ($vprasanja as $vpr){?>
                         <div class="col-md-12 col-sm-12">
                            <div class="single-blog two-column">
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html"><?php echo $vpr->naslov_vprasanja; ?></a></h2>
                                    <h3 class="post-author"><a href="#"><?php echo $uporabniki[$j][0]->uporabnisko_ime; ?></a></h3>
                                    <p><?php echo $vpr->besedilo; ?></p>
                                    <a href="#" class="read-more">View More</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Deli</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i><?php echo $vpr->ocenjenost; ?> Srčkov</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Komentarji</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><?php $j++;}?>
                        <!--<div class="col-md-12 col-sm-12">
                            <div class="single-blog two-column">
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html">Kam je šel beli zajček?</a></h2>
                                    <h3 class="post-author"><a href="#">Sprašuje lisička</a></h3>
                                    <p>Lisičko zanima kam je šel beli zajček</p>
                                    <a href="#" class="read-more">View More</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Deli</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Srčkov</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Komentarji</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><div class="col-md-12 col-sm-12">
                            <div class="single-blog two-column">
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html">Kam je šel beli zajček?</a></h2>
                                    <h3 class="post-author"><a href="#">Sprašuje lisička</a></h3>
                                    <p>Lisičko zanima kam je šel beli zajček</p>
                                    <a href="#" class="read-more">View More</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Deli</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Srčkov</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Komentarji</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="col-md-12 col-sm-12">
                            <div class="single-blog two-column">
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="blogdetails.html">Kam je šel beli zajček?</a></h2>
                                    <h3 class="post-author"><a href="#">Sprašuje lisička</a></h3>
                                    <p>Lisičko zanima kam je šel beli zajček</p>
                                    <a href="#" class="read-more">View More</a>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"><i class="fa fa-tag"></i>Deli</a></li>
                                            <li><a href="#"><i class="fa fa-heart"></i>32 Srčkov</a></li>
                                            <li><a href="#"><i class="fa fa-comments"></i>3 Komentarji</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    <div class="blog-pagination">
                        <ul class="pagination">
                          <li><a href="#">left</a></li>
                          <li><a href="#">1</a></li>
                          <li><a href="#">2</a></li>
                          <li class="active"><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#">7</a></li>
                          <li><a href="#">8</a></li>
                          <li><a href="#">9</a></li>
                          <li><a href="#">right</a></li>
                        </ul>
                    </div>
                 </div>
            </div>
        </div>
    </section>
    <!--/#blog-->

@endsection

