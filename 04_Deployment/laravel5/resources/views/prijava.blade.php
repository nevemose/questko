@extends('layouts.master')

@section('header')

@section('content')
<div class="contact-form bottom" style="width: 50%; margin:auto">
	<h2>Prijavi se</h2>
	<form id="main-contact" name="contact" method="post" action="prijava/1">
		<div class="form-group">
			<input type="text" name="uporabnisko_ime" class="form-control" required="required" placeholder="Uporabniško ime">
		</div>
		<div class="form-group">
			<input type="password" name="geslo" class="form-control" required="required" placeholder="Geslo">
		</div>
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-submit" value="Prijavi se">
		</div>
		<input name="_token" type="hidden" value="{!! csrf_token() !!}" />
		<a href="{{url()}}/registracija"><h3>Še nimaš računa? Registriraj se</h3></a>
	</form>
</div>
@endsection