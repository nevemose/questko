@extends('layouts.master')

@section('header')

@section('content')
<div class="contact-form bottom" style="width: 75%; margin:auto">
	<h2>Registracija</h2>
	<form id="main-" name="contact" method="request" action="registracija/1">
		<div class="form-group">
			<label for="sel1">Uporabniško ime</label>
			<input type="text" name="uporabnisko" class="form-control" required="required" placeholder="Uporabniško ime">
		</div>
		<div class="form-group">
			<label for="sel1">Elektronska pošta</label>
			<input type="text" name="elektronska_posta" class="form-control" required="required" placeholder="Elektronska pošta">
		</div>
		<div class="form-group">
			<label for="sel1">Geslo</label>
			<input type="password" name="geslo" class="form-control" required="required" placeholder="Geslo">
		</div>
		<div class="form-group">
			<label for="sel1">Ponovi geslo</label>
			<input type="password" name="geslo_p" class="form-control" required="required" placeholder="Ponovi geslo">
		</div>
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-submit" value="Registracija">
		</div>
	</form>
</div>
@endsection