@extends('layouts.master')

@section('header')

@section('content')

<div class="contact-form bottom">
	<h2>Postavi vprašanje</h2>
	<form id="main" name="contact" method="request" action="vprasaj/1">
		<div class="form-group">
			<label for="sel1">Vprašanje</label>
			<input type="text" name="name" class="form-control" required="required" placeholder="Vprašanje">
		</div>
		<div class="form-group">
			<label for="sel1">Kategorija</label>
			<select class="form-control" id="sel1">
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
			</select>
		</div>
		<div class="form-group">
			<label for="sel1">Opis</label>
			<textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Opis"></textarea>
		</div>
		<div class="form-group">
			<input type="submit" name="submit" class="btn btn-submit" value="Vprašaj">
		</div>
	</form>
</div>

@endsection
