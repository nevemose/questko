<?php

namespace App\Http\Controllers;

use App\Vprasanje;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Request;
use Hash;
use Session;

class SessionsController extends Controller
{
   
	public function create()
    {
	$input = Input::only('uporabnisko_ime','geslo');
	
	$uporabnisko = $input['uporabnisko_ime'];
	$g=md5($input['geslo']);
	
	$uporabniki= DB::table('uporabnik')->select('uporabnisko_ime')->where('uporabnisko_ime', $uporabnisko)->where('geslo', $g)->get();
	if(count($uporabniki)<=0){
		return redirect()->action('Front@prijava');
	}
	else{
		
		Session::put('uporabnisko_ime', $uporabnisko);
		return redirect()->action('VprasanjeController@index');
	}

    }
}