<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Front extends Controller
{
  
    public function index() {
        return view('index');
    }

    public function oprojektu() {
        return view('oprojektu');
    }

    public function odgovori(){
    	return view('odgovori');
    }
	
	public function vprasaj(){
    	return view('vprasaj');
    }
	
	public function prijava(){
    	return view('prijava');
    }
	
	public function registracija(){
    	return view('registracija');
    }

}
