-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Gostitelj: localhost
-- Čas nastanka: 16. dec 2015 ob 20.17
-- Različica strežnika: 10.0.17-MariaDB
-- Različica PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `questko`
--

-- --------------------------------------------------------

--
-- Struktura tabele `kategorija`
--

CREATE TABLE `kategorija` (
  `id_kategorija` int(11) NOT NULL,
  `naziv` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `kategorija`
--

INSERT INTO `kategorija` (`id_kategorija`, `naziv`) VALUES
(1, 'Računalništvo'),
(2, 'Poezija'),
(3, 'Ekonomija'),
(4, 'Kuhanje'),
(5, 'Zdravje'),
(6, 'Umetnost'),
(7, 'Sam svoj mojster'),
(8, 'Kriminal');

-- --------------------------------------------------------

--
-- Struktura tabele `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(11) NOT NULL,
  `besedilo` varchar(100) NOT NULL,
  `VPRASANJE_id_vprasanja` int(11) NOT NULL,
  `UPORABNIK_id_uporabnik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `odgovor`
--

CREATE TABLE `odgovor` (
  `id_odgovor` int(11) NOT NULL,
  `besedilo` varchar(100) NOT NULL,
  `VPRASANJE_id_vprasanja` int(11) NOT NULL,
  `UPORABNIK_id_uporabnik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `pripadanje`
--

CREATE TABLE `pripadanje` (
  `VPRASANJE_id_vprasanja` int(11) NOT NULL,
  `KATEGORIJA_id_kategorija` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `pripadnost_kategorije`
--

CREATE TABLE `pripadnost_kategorije` (
  `UPORABNIK_id_uporabnik` int(11) NOT NULL,
  `KATEGORIJA_id_kategorija` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabele `uporabnik`
--

CREATE TABLE `uporabnik` (
  `id_uporabnik` int(11) NOT NULL,
  `ime` varchar(45) NOT NULL,
  `priimek` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `uporabnisko_ime` varchar(45) NOT NULL,
  `geslo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `uporabnik`
--

INSERT INTO `uporabnik` (`id_uporabnik`, `ime`, `priimek`, `email`, `uporabnisko_ime`, `geslo`) VALUES
(1, 'spela', ' ', 'nekaj@gmail.com', 'spela', '12345'),
(2, 'nekdo', 'nekaj', 'marko_skace@gmail.com', 'marko', '$2y$10$6wqgW9UkpFInfsztTOtME.lT/kcCZx4MMSN1GK');

-- --------------------------------------------------------

--
-- Struktura tabele `vprasanje`
--

CREATE TABLE `vprasanje` (
  `id_vprasanja` int(11) NOT NULL,
  `naslov_vprasanja` varchar(200) NOT NULL,
  `besedilo` varchar(200) NOT NULL,
  `ocenjenost` int(11) NOT NULL,
  `UPORABNIK_id_uporabnik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `vprasanje`
--

INSERT INTO `vprasanje` (`id_vprasanja`, `naslov_vprasanja`, `besedilo`, `ocenjenost`, `UPORABNIK_id_uporabnik`) VALUES
(1, 'Ali bo letos sneg?', 'Zanima me, če bo letos do božiča zapadel sneg.', 5, 1),
(20, 'Kaj je to poledica?', 'Zanima me kaj je poledica, kdaj nastane ...', 8, 1);

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `kategorija`
--
ALTER TABLE `kategorija`
  ADD PRIMARY KEY (`id_kategorija`);

--
-- Indeksi tabele `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`),
  ADD KEY `fk_KOMENTAR_VPRASANJE` (`VPRASANJE_id_vprasanja`),
  ADD KEY `fk_KOMENTAR_UPORABNIK` (`UPORABNIK_id_uporabnik`);

--
-- Indeksi tabele `odgovor`
--
ALTER TABLE `odgovor`
  ADD PRIMARY KEY (`id_odgovor`),
  ADD KEY `fk_ODGOVOR_VPRASANJE1_idx` (`VPRASANJE_id_vprasanja`),
  ADD KEY `fk_ODGOVOR_UPORABNIK1_idx` (`UPORABNIK_id_uporabnik`);

--
-- Indeksi tabele `pripadanje`
--
ALTER TABLE `pripadanje`
  ADD PRIMARY KEY (`VPRASANJE_id_vprasanja`,`KATEGORIJA_id_kategorija`),
  ADD KEY `fk_VPRASANJE_has_KATEGORIJA_KATEGORIJA1_idx` (`KATEGORIJA_id_kategorija`),
  ADD KEY `fk_VPRASANJE_has_KATEGORIJA_VPRASANJE1_idx` (`VPRASANJE_id_vprasanja`);

--
-- Indeksi tabele `pripadnost_kategorije`
--
ALTER TABLE `pripadnost_kategorije`
  ADD PRIMARY KEY (`UPORABNIK_id_uporabnik`,`KATEGORIJA_id_kategorija`),
  ADD KEY `fk_UPORABNIK_has_KATEGORIJA_KATEGORIJA1_idx` (`KATEGORIJA_id_kategorija`),
  ADD KEY `fk_UPORABNIK_has_KATEGORIJA_UPORABNIK1_idx` (`UPORABNIK_id_uporabnik`);

--
-- Indeksi tabele `uporabnik`
--
ALTER TABLE `uporabnik`
  ADD PRIMARY KEY (`id_uporabnik`);

--
-- Indeksi tabele `vprasanje`
--
ALTER TABLE `vprasanje`
  ADD PRIMARY KEY (`id_vprasanja`),
  ADD KEY `fk_VPRASNJE_UPORABNIK` (`UPORABNIK_id_uporabnik`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabele `odgovor`
--
ALTER TABLE `odgovor`
  MODIFY `id_odgovor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabele `uporabnik`
--
ALTER TABLE `uporabnik`
  MODIFY `id_uporabnik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT tabele `vprasanje`
--
ALTER TABLE `vprasanje`
  MODIFY `id_vprasanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- Omejitve tabel za povzetek stanja
--

--
-- Omejitve za tabelo `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `fk_KOMENTAR_UPORABNIK` FOREIGN KEY (`UPORABNIK_id_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_KOMENTAR_VPRASANJE` FOREIGN KEY (`VPRASANJE_id_vprasanja`) REFERENCES `vprasanje` (`id_vprasanja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `odgovor`
--
ALTER TABLE `odgovor`
  ADD CONSTRAINT `fk_ODGOVOR_UPORABNIK1` FOREIGN KEY (`UPORABNIK_id_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ODGOVOR_VPRASANJE1` FOREIGN KEY (`VPRASANJE_id_vprasanja`) REFERENCES `vprasanje` (`id_vprasanja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `pripadanje`
--
ALTER TABLE `pripadanje`
  ADD CONSTRAINT `fk_VPRASANJE_has_KATEGORIJA_KATEGORIJA1` FOREIGN KEY (`KATEGORIJA_id_kategorija`) REFERENCES `kategorija` (`id_kategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_VPRASANJE_has_KATEGORIJA_VPRASANJE1` FOREIGN KEY (`VPRASANJE_id_vprasanja`) REFERENCES `vprasanje` (`id_vprasanja`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `pripadnost_kategorije`
--
ALTER TABLE `pripadnost_kategorije`
  ADD CONSTRAINT `fk_PRIPADNOST_KATEGORIJE_KATEGORIJA` FOREIGN KEY (`KATEGORIJA_id_kategorija`) REFERENCES `kategorija` (`id_kategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PRIPADNOST_KATEGORIJE_UPORABNIK` FOREIGN KEY (`UPORABNIK_id_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Omejitve za tabelo `vprasanje`
--
ALTER TABLE `vprasanje`
  ADD CONSTRAINT `fk_VPRASANJE_UPORABNIK1` FOREIGN KEY (`UPORABNIK_id_uporabnik`) REFERENCES `uporabnik` (`id_uporabnik`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
